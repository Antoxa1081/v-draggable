import Vue from 'vue';
import App from './App.vue';

import VDraggable from 'v-draggable';

Vue.config.productionTip = false;

Vue.use(VDraggable);

new Vue({
  render: h => h(App),
}).$mount('#app');
