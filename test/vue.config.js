const path = require("path")

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  baseUrl: "./",
  runtimeCompiler: true,

  chainWebpack: config => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("v-draggable", resolve("../"));
  }
};
