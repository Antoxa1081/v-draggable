import DraggEmitter from "./draggEmitter";

export default {
  name: "VListDraggble",

  props: {
    value: {
      type: Array,
      default: []
    }
  },

  data() {
    return {
      uniqueId: null,

      draggElem: {
        link: null,
        offset: {
          x: 0,
          y: 0
        }
      }
    };
  },

  watch: {
    value: {
      handler() {
        this.$nextTick(function() {
          this.watchChildren();
        });
      },
      deep: true
    }
  },

  methods: {
    resetDraggElem() {
      this.draggElem.link.removeAttribute("style");

      this.draggElem.link = null;
      this.draggElem.offset.x = 0;
      this.draggElem.offset.y = 0;
    },

    addItem(data) {
      let newArr = [].concat(this.value);
      newArr.push(data);

      this.$emit("input", newArr);
    },

    removeItem(index) {
      let newArr = [].concat(this.value);
      newArr.splice(index, 1);

      this.$emit("input", newArr);
    },

    findParentFromChildByClass(element, classname) {
      if(!element) {
        return null;
      }

      if(element.classList && element.classList.contains(classname)) {
        return element;
      } else if(element.parentNode) {
        return this.findParentFromChildByClass(element.parentNode, classname);   
      } else {
        return null;
      }
    },

    watchChildren() {
      this.$el.childNodes.forEach((draggItem) => {
        if (draggItem.classList.contains("dragg-elem")) {
          return null;
        }

        draggItem.classList.add("dragg-elem");

        const
          mouseMove = (event) => {
            this.posDraggElem = event;
          },

          mouseUp = (event) => {
            this.draggElem.link.classList.remove("ghost");
            this.draggElem.link.style.display = "none";
            
            document.removeEventListener("mousemove", mouseMove);
            document.removeEventListener("mouseup", mouseUp);

            const draggComponent = this.findParentFromChildByClass(
              document.elementFromPoint(event.clientX, event.clientY),
              "v-list-draggable"
            );

            if (draggComponent) {
              const draggId = draggComponent.getAttribute("data-v-dragg");

              // Dragg end on parent component
              if (draggId != this.uniqueId) {
                const index = this.draggElem.link.getAttribute("data-key");
                DraggEmitter.emit(draggId + ":move", this.value[index]);

                this.removeItem(index);
              }
            }

            this.resetDraggElem();
          };

        draggItem.addEventListener('mousedown', (event) => {
          this.draggElem.link = draggItem;

          this.draggElem.offset.x = this.posDraggElem.x - event.clientX;
          this.draggElem.offset.y = this.posDraggElem.y - event.clientY;

          this.draggElem.link.style.position = "absolute";

          document.addEventListener("mousemove", mouseMove);
          document.addEventListener("mouseup", mouseUp);

          this.posDraggElem = event;
          this.draggElem.link.classList.add("ghost");
        }, true);
      });
    }
  },

  mounted() {
		this.uniqueId = Math.random().toString(36).substr(2, 16);

    this.watchChildren();

    DraggEmitter.subscribe(`${this.uniqueId}:move`, (data) => {
      this.addItem(data);
    });
  },

  computed: {
    posDraggElem: {
      get() {
        return {
          x: this.draggElem.link.offsetLeft,
          y: this.draggElem.link.offsetTop
        };
      },

      set(event) {
        this.draggElem.link.style.left = event.clientX + this.draggElem.offset.x + "px";
        this.draggElem.link.style.top = event.clientY + this.draggElem.offset.y + "px";
      }
    }
  },

  template: `
    <div class="v-list-draggable" :data-v-dragg="uniqueId">
      <slot></slot>
    </div>
  `
};
