/**
 * Lavender magic
 */

import DragComponent from './draggComponent';


/**
 * General class. Injected in Vue instance
 */
class Plugin {
    /**
     * Required function for vue plugin 
     * 
     * @param {Vue} Vue 
     * @param {Object} langs 
     * @param {String} defaultLang 
     */
    install(Vue) {
        Vue.component('v-list-draggable', DragComponent);
    }
}


export default new Plugin();
